import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './../src/app.module';
import { Connection } from 'mongoose';
import * as mongoose from 'mongoose';
import * as request from 'supertest';
import { UserSchema } from '../src/user/schemas/user.schema';

//https://www.codota.com/code/javascript/functions/supertest/Test/set

let app: INestApplication;
let db: Connection;
let configService: ConfigService;
let token;

beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();
  try {
    app = moduleFixture.createNestApplication();
    await app.init();
    configService = app.get<ConfigService>(ConfigService);
    console.log("database:"+ configService.get<string>('connectDB'));
    db =await mongoose.createConnection(configService.get<string>('connectDB'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    db.model('users', UserSchema);
  } catch (error) {
    console.log(error);
  }
},60000);

afterAll(async () => {
  await db.model('users').deleteMany({});
  await db.close();
  await app.close();
}, 60000);

describe('AppController (e2e)', () => {
  describe('test AuthController', () => {
    const requestRegister = {
      email: 'ngoc861999@gmail.com',
      password: 'a123456',
      first_name: 'Ngoc',
      last_name: 'Dinh',
    };

    const requestLogin = {
      email: 'ngoc861999@gmail.com',
      password: 'a123456',
    };

    it('/auth/register badrequest', () => {
      return request(app.getHttpServer())
        .post('/auth/register')
        .send({
          ...requestRegister,
          password: '1',
        })
        .expect(400);
    });

    it('/auth/register success', () => {
      return request(app.getHttpServer())
        .post('/auth/register')
        .send(requestRegister)
        .expect(201)
        .then((res) => {
          token = res.body.access_token;
        });
    });

    it('/auth/register confict', () => {
      return request(app.getHttpServer())
        .post('/auth/register')
        .send(requestRegister)
        .expect(409);
    });

    it('/auth/login success', () => {
      return request(app.getHttpServer())
        .post('/auth/login')
        .send(requestLogin)
        .expect(201);
    });

    it('/auth/login fail', () => {
      return request(app.getHttpServer())
        .post('/auth/login')
        .send({
          email: 'ngoc86199@gmail.com',
          password: '12345',
        })
        .expect(400);
    });
  });

  describe('test UserController', () => {
    it('/user retrive fail', () => {
      return request(app.getHttpServer())
        .get('/user')
        .set('token', '1235')
        .expect(401);
    });

    it('/user retrive success', () => {
      return request(app.getHttpServer())
        .get('/user')
        .set('token', token)
        .expect(200);
    });

    it('update user fail authorization ', () => {
      return request(app.getHttpServer()).patch('/user').expect(401);
    });

    it('update user fail bad request ', () => {
      return request(app.getHttpServer())
        .patch('/user')
        .set('token', token)
        .send({
          phone: '123',
        })
        .expect(400);
    });

    it('update user success', () => {
      return request(app.getHttpServer())
        .patch('/user')
        .set('token', token)
        .send()
        .expect(200);
    });
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});
