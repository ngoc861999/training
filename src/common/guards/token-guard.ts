import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';
import * as mongoose from 'mongoose';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    if (request.headers.token) {
      try {
        const authorization = this.jwtService.verify(request.headers.token);
        if (!mongoose.Types.ObjectId.isValid(authorization.userId)) {
          throw new UnauthorizedException();
        }
        request.userId = authorization.userId;
        return true;
      } catch (error) {}
    }
    throw new UnauthorizedException();
  }
}
