export default () => ({
  port: parseInt(process.env.PORT, 10) || 8080,
  secret_token: process.env.SECRET_TOKEN,
  connectDB: process.env.CONNECT_DB,
});
