//https://github.com/typestack/class-validator#validation-decorators
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { IsPassword } from '../../user/decorator/user-dto';

export class LoginDTO {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsPassword()
  password: string;
}
