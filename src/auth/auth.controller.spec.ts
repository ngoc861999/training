import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserService } from './../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcryptjs from 'bcryptjs';

jest.mock('bcryptjs');

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;
  let userService: UserService;
  let bcryptjsCompare: jest.Mock;

  const user = {
    _id: '123456',
    first_name: 'Ngọc',
    last_name: 'Đinh',
    email: 'ngoc86199@gmail.com',
    phone: '+84395297295',
    password: '123456',
    address: '',
    status: true,
  };

  const response = {
    access_token: 'access_token',
    first_name: 'Ngọc',
  };

  const mockUserService = {
    createUser: jest.fn().mockResolvedValue(user),
    getUserByEmail: jest.fn().mockResolvedValue(user),
  };

  const mockedJwtService = {
    sign: () => 'access_token',
  };

  beforeEach(async () => {
    bcryptjsCompare = jest.fn().mockReturnValue(true);
    (bcryptjs.compare as jest.Mock) = bcryptjsCompare;

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: mockUserService,
        },
        {
          provide: JwtService,
          useValue: mockedJwtService,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('test register', () => {
    it('test with correct data', async () => {
      expect(await controller.registerUser(user)).toEqual(response);
      expect(userService.createUser).toBeCalledWith(user);
    });
  });

  describe('test login', () => {
    beforeEach(() => {
      bcryptjsCompare.mockReturnValue(true);
    });
    it('test with correct data', async () => {
      expect(await controller.loginUser(user)).toEqual(response);
      expect(userService.getUserByEmail).toBeCalledWith(user.email);
    });
  });
});
