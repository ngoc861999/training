import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcryptjs from 'bcryptjs';
import { CreateUserDTO } from '../user/dto/create-user.dto';
import { UserService } from '../user/user.service';
import { LoginDTO } from './dto/login.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  //GENERATE JWT
  private genarateToken(id: string, first_name: string) {
    const payload = { userId: id };
    const access_token = this.jwtService.sign(payload);
    return { access_token, first_name: first_name };
  }

  //COMPARE HASH PASSWORD
  private async comparePassword(pw: string, hashPw: string) {
    const compare = await bcryptjs.compare(pw, hashPw);
    if (!compare) throw new BadRequestException('Email or Password invalid');
  }

  //REGISTER USER
  async registerUser(createUserDTO: CreateUserDTO): Promise<any> {
    //Create new user
    const user = await this.userService.createUser(createUserDTO);
    return this.genarateToken(user._id, user.first_name);
  }

  //LOGIN USER
  async loginUser(loginDTO: LoginDTO): Promise<any> {
    //Check email valid
    const user = await this.userService.getUserByEmail(loginDTO.email);

    //check password valid
    await this.comparePassword(loginDTO.password, user.password);
    return this.genarateToken(user._id, user.first_name);
  }
}
