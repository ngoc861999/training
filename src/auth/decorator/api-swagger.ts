import { applyDecorators } from '@nestjs/common';
import { CreateUserDTO } from '../../user/dto/create-user.dto';
import { LoginDTO } from './../dto/login.dto';
import {
  ApiBody,
  ApiTags,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
} from '@nestjs/swagger';

export function ApiRegister() {
  return applyDecorators(
    ApiTags('Auth'),
    ApiCreatedResponse(),
    ApiConflictResponse(),
    ApiBadRequestResponse(),
    ApiBody({ type: CreateUserDTO }),
  );
}

export function ApiLogin() {
  return applyDecorators(
    ApiTags('Auth'),
    ApiOkResponse(),
    ApiNotFoundResponse(),
    ApiBadRequestResponse(),
    ApiBody({ type: LoginDTO }),
  );
}
