import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDTO } from '../user/dto/create-user.dto';
import { LoginDTO } from './dto/login.dto';
import { ApiRegister, ApiLogin } from './decorator/api-swagger';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  // REGISTER USER
  @ApiRegister()
  @Post('register')
  async registerUser(@Body() registerDTO: CreateUserDTO) {
    const registerSuccess = await this.authService.registerUser(registerDTO);
    return registerSuccess;
  }

  // LOGIN USER
  @ApiLogin()
  @Post('login')
  async loginUser(@Body() loginDTO: LoginDTO) {
    const loginSuccess = await this.authService.loginUser(loginDTO);
    return loginSuccess;
  }
}
