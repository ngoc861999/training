import { getModelToken } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import * as bcryptjs from 'bcryptjs';
import { UserService } from './../user/user.service';

jest.mock('bcryptjs');

describe('AuthService', () => {
  let authService: AuthService;
  let userService: UserService;
  let bcryptjsCompare: jest.Mock;

  const user = {
    first_name: 'Ngọc',
    last_name: 'Đinh',
    email: 'ngoc86199@gmail.com',
    phone: '+84395297295',
    password: '123456',
    address: '',
    status: true,
  };

  const response = {
    access_token: 'access_token',
    first_name: 'Ngọc',
  };

  const mockUserModel = {
    findOne: jest.fn().mockResolvedValue(user),
  };

  const mockUserService = {
    createUser: jest.fn().mockResolvedValue(user),
    getUserByEmail: jest.fn().mockResolvedValue(user),
  };

  const mockedJwtService = {
    sign: () => 'access_token',
  };

  beforeAll(async () => {
    bcryptjsCompare = jest.fn().mockReturnValue(true);
    (bcryptjs.compare as jest.Mock) = bcryptjsCompare;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: mockUserService,
        },
        {
          provide: JwtService,
          useValue: mockedJwtService,
        },
        {
          provide: getModelToken('user'),
          useValue: mockUserModel,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
  });

  describe('login with username- password valid', () => {
    beforeEach(() => {
      bcryptjsCompare.mockReturnValue(true);
    });
    it('should return  access_token and first_name', async () => {
      expect(
        await authService.loginUser({ email: 'email', password: 'password' }),
      ).toEqual(response);
    });
  });

  describe('login with username- password invalid', () => {
    beforeEach(() => {
      bcryptjsCompare.mockReturnValue(false);
    });
    it('should return  error', async () => {
      await expect(
        authService.loginUser({ email: 'email', password: 'password' }),
      ).rejects.toThrow();
    });
  });

  describe('register user', () => {
    it('login with username- password invalid', async () => {
      expect(await authService.registerUser(user)).toEqual(response);
    });
  });
});
