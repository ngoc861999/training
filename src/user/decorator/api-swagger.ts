import { applyDecorators } from '@nestjs/common';
import { CreateUserDTO } from '../../user/dto/create-user.dto';
import { ApiTags, ApiNotFoundResponse, ApiOkResponse } from '@nestjs/swagger';

export function ApiRetriveUser() {
  return applyDecorators(
    ApiTags('User'),
    ApiOkResponse({ type: [CreateUserDTO] }),
  );
}

export function ApiUpdateUser() {
  return applyDecorators(
    ApiTags('User'),
    ApiOkResponse({ type: [CreateUserDTO] }),
    ApiNotFoundResponse(),
  );
}
