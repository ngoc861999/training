import { IsString, Length } from 'class-validator';
import { applyDecorators } from '@nestjs/common';

export function IsPassword() {
  return applyDecorators(IsString(), Length(6, 50));
}

export function IsName() {
  return applyDecorators(IsString(), Length(1, 30));
}
