import {
  Injectable,
  ConflictException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcryptjs from 'bcryptjs';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';
import { UpdateUserDTO } from './dto/update-user.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel('user') private readonly userModel: Model<User>) {}

  //RETRIVE USER BY ID
  async getUserById(idUser: string): Promise<User> {
    const user = await await this.userModel.findById(idUser);
    if (!user) throw new NotFoundException('Not found user');
    return user;
  }

  //RETRIVE USER BY EMAIL
  async getUserByEmail(email: string): Promise<User> {
    const user = await this.userModel.findOne({ email: email });
    if (!user) throw new NotFoundException('Not found user');
    return user;
  }

  //CREATE USER
  async createUser(createUserDTO: CreateUserDTO): Promise<User> {
    //Check email exist. Check unique email in schema?????
    const checkEmail = await this.userModel.findOne({
      email: createUserDTO.email,
    });
    if (checkEmail) throw new ConflictException('Email already exists!');

    //Hash password and save to database
    const hash = await bcryptjs.hash(createUserDTO.password, 10);
    const newCustomer = await this.userModel.create({
      ...createUserDTO,
      password: hash,
    });

    return newCustomer.save();
  }

  //UPDATE USER
  async updateUser(id: string, updateUserDTO: UpdateUserDTO): Promise<User> {
    //Check user exist
    const user = await this.userModel.findById(id);
    if (!user) throw new NotFoundException('Not found user');

    //Update data
    if (updateUserDTO.first_name) user.first_name = updateUserDTO.first_name;
    if (updateUserDTO.last_name) user.last_name = updateUserDTO.last_name;
    if (updateUserDTO.phone) user.phone = updateUserDTO.phone;
    if (updateUserDTO.address) user.first_name = updateUserDTO.address;

    //Update user
    return await this.userModel.findByIdAndUpdate(user._id, user);
  }
}
