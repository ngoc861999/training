import {
  UseGuards,
  Controller,
  Get,
  Req,
  Body,
  Patch,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UpdateUserDTO } from './dto/update-user.dto';
import { ApiRetriveUser, ApiUpdateUser } from './decorator/api-swagger';
import { UserResponse } from './dto/user-response.dto';
import { TokenGuard } from '../common/guards/token-guard';

@UseGuards(TokenGuard)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  // RETRIVE USER
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiRetriveUser()
  @Get()
  async retriveUser(@Req() req): Promise<UserResponse> {
    const user = await this.userService.getUserById(req.userId);
    const res = new UserResponse(user);
    return res;
  }

  // UPDATE USER
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiUpdateUser()
  @Patch()
  async updateUser(
    @Req() req,
    @Body() updateUserDTO: UpdateUserDTO,
  ): Promise<UserResponse> {
    const updateSuccess = await this.userService.updateUser(
      req.userId,
      updateUserDTO,
    );
    const res = new UserResponse(updateSuccess);
    return res;
  }
}
