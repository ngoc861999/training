import { Document } from 'mongoose';

export class User extends Document {
  readonly email: string;
  first_name: string;
  last_name: string;
  phone: string;
  address: string;
  password: string;
  status: boolean;
}
