import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String,
  phone: String,
  password: String,
  address: String,
  status: {
    type: Boolean,
    default: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});
