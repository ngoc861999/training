import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { getModelToken } from '@nestjs/mongoose';
import { createRequest } from 'node-mocks-http';
import { TokenGuard } from '../common/guards/token-guard';

describe('UserController', () => {
  let controller: UserController;
  let userService: UserService;

  const user = {
    _id: '5fcda81a88a849408c6da0dc',
    first_name: 'Ngọc',
    last_name: 'Đinh',
    email: 'ngoc86199@gmail.com',
    phone: '+84395297295',
    password: '123456',
    create_date: '2020-12-07T03:56:19.708Z',
    status: true,
  };

  const updateData = {
    id: '5fcda81a88a849408c6da0dc',
    first_name: 'Ngọc',
    last_name: 'Đinh',
    phone: '+84395297295',
    address: '',
  };

  const mockUserModel = {
    findById: jest.fn().mockResolvedValue(user),
    findByIdAndUpdate: jest.fn().mockResolvedValue(user),
  };

  const mockGuard = {
    canActivate: jest.fn(() => true),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getModelToken('user'),
          useValue: mockUserModel,
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue(mockGuard)
      .compile();

    controller = module.get<UserController>(UserController);
    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('test retrive user', () => {
    const req = createRequest();
    req.userId = '123456';
    it('retrive user with correct id', async () => {
      await controller.retriveUser(req);
      expect(mockUserModel.findById).toBeCalledWith('123456');
    });
  });

  describe('test update user', () => {
    const req = createRequest();
    req.userId = '5fcda81a88a849408c6da0dc';
    it('test update user with correct data', async () => {
      await controller.updateUser(req, updateData);
      expect(mockUserModel.findByIdAndUpdate).toBeCalledWith(user._id, user);
    });
  });
});
