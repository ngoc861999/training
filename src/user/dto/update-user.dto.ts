import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsName } from '../decorator/user-dto';
import {
  IsOptional,
  IsPhoneNumber,
  IsString,
  MaxLength,
} from 'class-validator';

export class UpdateUserDTO {
  @ApiPropertyOptional()
  @IsOptional()
  @IsName()
  first_name: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsName()
  last_name: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsPhoneNumber('+84')
  phone: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  @MaxLength(50)
  address: string;
}
