import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsName, IsPassword } from '../decorator/user-dto';
import {
  IsEmail,
  IsOptional,
  IsPhoneNumber,
  IsString,
  MaxLength,
} from 'class-validator';

export class CreateUserDTO {
  @ApiProperty()
  @IsName()
  first_name: string;

  @ApiProperty()
  @IsName()
  last_name: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsPassword()
  password: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsPhoneNumber('+84')
  phone: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  @MaxLength(50)
  address: string;

  @ApiPropertyOptional()
  @IsOptional()
  status: boolean;
}
