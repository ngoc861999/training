import { Exclude, Type } from 'class-transformer';
import { Document } from 'mongoose';

export class UserResponse {
  @Type(() => String)
  _id: string;

  status: boolean;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;

  @Exclude()
  password: string;

  constructor(partial: Partial<UserResponse>) {
    try {
      Object.assign(this, (partial as Document).toJSON({}));
    } catch (error) {
      console.log('try catch because typeof(partial) is object');
    }
  }
}
