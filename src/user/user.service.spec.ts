import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { UserService } from './user.service';
import * as bcryptjs from 'bcryptjs';

jest.mock('bcryptjs');

describe('UserService', () => {
  let service: UserService;
  let bcryptjsHash: jest.Mock;

  const user = {
    first_name: 'Ngọc',
    last_name: 'Đinh',
    email: 'ngoc86199@gmail.com',
    phone: '+84395297295',
    password: '123456',
    address: '',
    status: true,
  };

  const mockUserModel = {
    findById: jest.fn().mockResolvedValue(user),
    findOne: jest.fn().mockResolvedValue(null),
    create: jest.fn().mockResolvedValue({
      save: jest.fn().mockResolvedValue(user),
    }),
    findByIdAndUpdate: jest.fn().mockResolvedValue(user),
  };

  beforeEach(async () => {
    bcryptjsHash = jest.fn().mockResolvedValue('123456');
    (bcryptjs.hash as jest.Mock) = bcryptjsHash;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getModelToken('user'),
          useValue: mockUserModel,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('test get user by Id', async () => {
    expect(await service.getUserById('123')).toEqual(user);
    expect(mockUserModel.findById).toBeCalledWith('123');
  });

  describe('Create user', () => {
    beforeEach(() => {
      bcryptjsHash = jest.fn().mockResolvedValue('123456');
    });
    it('test create user', async () => {
      expect(await service.createUser(user)).toEqual(user);
      expect(mockUserModel.findOne).toBeCalledWith({ email: user.email });
      expect(mockUserModel.create).toBeCalledWith(user);
    });
  });

  describe('Update user', () => {
    it('test create user', async () => {
      expect(await service.updateUser('123', user)).toEqual(user);
      expect(mockUserModel.findById).toBeCalledWith('123');
    });
  });
});
